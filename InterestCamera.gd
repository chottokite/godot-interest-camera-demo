extends Spatial

enum Mode { Orbit, Track }

export (Mode) var mode = Mode.Orbit setget set_mode
export (NodePath) var interest setget set_interest
export (bool) var follow = false setget set_follow
export (bool) var enable_input = false setget set_enable_input
export (float) var min_distance = 1

signal changed_follow_state(new_state)

var orbit_offset: Vector3
var orbit_rotation: Quat

onready var parent: Spatial = get_parent()
var target: Spatial

var target_position: Vector3 = Vector3()

var is_rotating = false
var is_panning = false

var pan_scroll_step = 1
var pan_movement_factor = 0.05
var orbit_movement_factor = 0.005

func _ready():
	get_target()
	update_orbit_rotation()
	update_orbit_offset()
	orbit_rotation = transform.basis.get_rotation_quat()
	set_enable_input(enable_input)

func _process(_delta):
	if follow and target:
		target_position = parent.to_local(target.to_global(Vector3()))
		
	if mode == Mode.Orbit:
		self.translation = target_position + orbit_offset
	
	elif mode == Mode.Track:
		look_at_target_position(Vector3.UP)

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if mode == Mode.Orbit:
			var pan = 0
			if event.button_index == BUTTON_RIGHT:
				is_rotating = event.pressed
				is_panning = false
			elif event.button_index == BUTTON_WHEEL_UP:
				pan = -pan_scroll_step
			elif event.button_index == BUTTON_WHEEL_DOWN:
				pan = pan_scroll_step
			if pan != 0:
				var new_offset = orbit_offset + orbit_offset.normalized() * pan
				if orbit_offset.dot(new_offset) > min_distance:
					orbit_offset = new_offset
		
		if mode == Mode.Track:
			if event.button_index == BUTTON_MIDDLE:
				is_rotating = false
				# ignore input and keep following
				# is_panning = not follow and event.pressed
				# or disable follow mode
				set_follow(false)
				is_panning = event.pressed
			elif event.button_index == BUTTON_RIGHT:
				set_follow(false)
				is_rotating = event.pressed
				is_panning = false
	
	if event is InputEventMouseMotion:
		if is_rotating:
			var relative = event.relative * orbit_movement_factor
			if mode == Mode.Track:
				relative = -relative
			var b = transform.basis.rotated(Vector3.UP, relative.x)
			transform.basis = b.rotated(b.x, relative.y)
			if mode == Mode.Orbit:
				var distance = orbit_offset.length()
				orbit_offset = transform.basis.xform(Vector3(0, 0, distance))
				self.translation = target_position + orbit_offset
				get_tree().set_input_as_handled()
			
		if is_panning:
			var side = transform.basis.xform(Vector3(-event.relative.x, 0, 0))
			var depth = transform.basis.xform(Vector3.FORWARD)
			depth.y = 0
			transform.origin = translation + (side + depth.normalized() * event.relative.y) * pan_movement_factor
			get_tree().set_input_as_handled()

func get_target():
	target = get_node_or_null(interest)

func look_at_target_position(up: Vector3):
	look_at(target_position, up)

func set_follow(new_state):
	if (follow != new_state):
		emit_signal("changed_follow_state", new_state)
	follow = new_state
	set_process(follow)

func set_interest(new_interest):
	interest = new_interest
	get_target()

func set_mode(new_mode):
	mode = new_mode
	update_orbit_rotation()
	update_orbit_offset()

func set_enable_input(enable):
	enable_input = enable
	if not enable_input:
		set_process_input(false)
		set_process_unhandled_input(false)
	else:
		set_process_unhandled_input(true)

func update_orbit_offset():
	if mode == Mode.Orbit:
		orbit_offset = translation - target_position

func update_orbit_rotation():
	if mode == Mode.Orbit:
		look_at_target_position(Vector3.UP)
